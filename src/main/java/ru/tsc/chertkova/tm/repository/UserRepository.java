package ru.tsc.chertkova.tm.repository;

import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.api.repository.IUserRepository;
import ru.tsc.chertkova.tm.model.User;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Nullable
    @Override
    public User findByLogin(@Nullable final String login) {
        return models.stream()
                .filter(item -> login.equals(item.getLogin()))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public User findByEmail(@Nullable final String email) {
        return models.stream()
                .filter(item -> email.equals(item.getEmail()))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public User removeByLogin(@Nullable final String login) {
        @Nullable final User user = findByLogin(login);
        if (user == null) return null;
        return remove(user);
    }

}
