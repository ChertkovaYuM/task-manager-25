package ru.tsc.chertkova.tm.repository;

import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.api.repository.IUserOwnerRepository;
import ru.tsc.chertkova.tm.enumerated.Sort;
import ru.tsc.chertkova.tm.model.AbstractUserOwnerModel;

import java.util.*;
import java.util.stream.Collectors;

public abstract class AbstractUserOwnerRepository<M extends AbstractUserOwnerModel> extends AbstractRepository<M>
        implements IUserOwnerRepository<M> {

    @Nullable
    @Override
    public M add(@Nullable final String userId, @Nullable final M model) {
        if (userId == null) return null;
        model.setUserId(userId);
        return add(model);
    }

    @Override
    public void clear(@Nullable final String userId) {
        findAll(userId).clear();
    }

    @Nullable
    @Override
    public List<M> findAll(@Nullable final String userId) {
        return models.stream()
                .filter(item -> userId.equals(item.getUserId()))
                .collect(Collectors.toList());
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        return findById(userId, id) != null;
    }

    @Nullable
    @Override
    public M findById(@Nullable final String userId, @Nullable final String id) {
        return models.stream()
                .filter(item -> id.equals(item.getId()) &&
                        userId.equals(item.getUserId()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public int getSize(@Nullable final String userId) {
        return models.stream()
                .filter(item -> userId.equals(item.getUserId()))
                .toArray().length;
    }

    @Nullable
    @Override
    public M remove(@Nullable final String userId, @Nullable final M model) {
        if (userId == null || model == null) return null;
        return removeById(userId, model.getId());
    }

    @Nullable
    @Override
    public M removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || id == null) return null;
        final M model = findById(userId, id);
        if (model == null) return null;
        return remove(model);
    }

    @Nullable
    @Override
    public List<M> findAll(@Nullable final String userId, @Nullable final Comparator<M> comparator) {
        return models.stream()
                .filter(item -> userId.equals(item.getUserId()))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Nullable
    @Override
    public List<M> findAll(@Nullable final String userId, @Nullable final Sort sort) {
        return models.stream()
                .filter(item -> userId.equals(item.getUserId()))
                .sorted((Comparator<? super M>) sort.getComparator())
                .collect(Collectors.toList());
    }

}
